/**
 * Created by Demon on 20.07.2014.
 */
package com
{
    import com.controller.commands.CheckUserInputCommand;
    import com.controller.commands.FacebookShareCommand;
    import com.controller.commands.GameOverCommand;
    import com.controller.commands.LevelCompleteCommand;
    import com.controller.commands.ResetGameCommand;
    import com.controller.commands.ShowLevelIntroCommand;
    import com.controller.commands.StartGameCommand;
    import com.controller.signals.CheckUserInputSignal;
    import com.controller.signals.ClearLevelSignal;
    import com.controller.signals.FBShareSignal;
    import com.controller.signals.GameOverSignal;
    import com.controller.signals.LevelCompleteSignal;
    import com.controller.signals.LevelPreparedSignal;
    import com.controller.signals.LivesUpdatedSignal;
    import com.controller.signals.ResetGameSignal;
    import com.controller.signals.RightCircleSelectedSignal;
    import com.controller.signals.ShowLevelIntroSignal;
    import com.controller.signals.StartGameSignal;
    import com.controller.signals.WrongCircleSelectedSignal;
    import com.model.AdsModel;
    import com.model.AppModel;
    import com.model.FacebookModel;
    import com.model.LevelModel;
    import com.model.SoundModel;
    import com.model.UserModel;
    import com.model.interfaces.IAdsModel;
    import com.model.interfaces.IAppModel;
    import com.model.interfaces.IFacebookModel;
    import com.model.interfaces.ILevelModel;
    import com.model.interfaces.ISoundModel;
    import com.model.interfaces.IUserModel;
    import com.service.social.facebook.FacebookService;
    import com.service.social.facebook.IFacebookService;
    import com.service.sounds.ISoundService;
    import com.service.sounds.SoundService;
    import com.view.GameOverView;
    import com.view.GameView;
    import com.view.LevelCompleteView;
    import com.view.LevelIntroView;
    import com.view.MenuView;
    import com.view.mediator.GameMediator;
    import com.view.mediator.GameOverMediator;
    import com.view.mediator.LevelCompleteMediator;
    import com.view.mediator.LevelIntroMediator;
    import com.view.mediator.MenuMediator;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IInjector;

    public class GameConfig implements IConfig
    {
        [Inject]
        public var injector:IInjector;

        [Inject]
        public var mediatorMap:IMediatorMap;

        [Inject]
        public var commandMap:ISignalCommandMap;

        [Inject]
        public var contextView:ContextView;

        public function GameConfig()
        {
            super();
        }

        public function configure():void
        {
            mapSignals();
            mapModels();
            mapCommands();
            mapServices();
            mapViews();

            onInitComplete();
        }

        private function mapSignals():void
        {
            injector.map(LevelPreparedSignal).asSingleton();
            injector.map(CheckUserInputSignal).asSingleton();
            injector.map(LivesUpdatedSignal).asSingleton();
            injector.map(GameOverSignal).asSingleton();
            injector.map(ShowLevelIntroSignal).asSingleton();
            injector.map(StartGameSignal).asSingleton();
            injector.map(RightCircleSelectedSignal).asSingleton();
            injector.map(WrongCircleSelectedSignal).asSingleton();
            injector.map(LevelCompleteSignal).asSingleton();
            injector.map(ClearLevelSignal).asSingleton();
            injector.map(ResetGameSignal).asSingleton();
        }

        private function mapModels():void
        {
            injector.map(IUserModel).toSingleton(UserModel);
            injector.map(ILevelModel).toSingleton(LevelModel);
            injector.map(IAdsModel).toSingleton(AdsModel);
            injector.map(IFacebookModel).toSingleton(FacebookModel);
            injector.map(ISoundModel).toSingleton(SoundModel);
            injector.map(IAppModel).toSingleton(AppModel);
        }

        private function mapServices():void
        {
            injector.map(IFacebookService).toSingleton(FacebookService);
            injector.map(ISoundService).toSingleton(SoundService);
        }

        private function mapCommands():void
        {
            commandMap.map(StartGameSignal).toCommand(StartGameCommand);
            commandMap.map(ShowLevelIntroSignal).toCommand(ShowLevelIntroCommand);
            commandMap.map(CheckUserInputSignal).toCommand(CheckUserInputCommand);
            commandMap.map(GameOverSignal).toCommand(GameOverCommand);
            commandMap.map(LevelCompleteSignal).toCommand(LevelCompleteCommand);
            commandMap.map(ResetGameSignal).toCommand(ResetGameCommand);
            commandMap.map(FBShareSignal).toCommand(FacebookShareCommand);
        }

        private function mapViews():void
        {
            mediatorMap.map(GameView).toMediator(GameMediator);
            mediatorMap.map(MenuView).toMediator(MenuMediator);
            mediatorMap.map(LevelIntroView).toMediator(LevelIntroMediator);
            mediatorMap.map(GameOverView).toMediator(GameOverMediator);
            mediatorMap.map(LevelCompleteView).toMediator(LevelCompleteMediator);
        }

        private function onInitComplete():void
        {
            showMenu();
        }

        private function showMenu():void
        {
            contextView.view.addChild(new MenuView());
        }
    }
}
