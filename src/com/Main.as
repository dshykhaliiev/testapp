/**
 * Created by Demon on 20.07.2014.
 */
package com
{
    import flash.display.Sprite;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;

    import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.impl.Context;

    [SWF(width='640',height='480')]
    public class Main extends Sprite
    {
        private var str:String;
        private var _context:IContext;

        public function Main()
        {
            if (stage)
            {
                stage.scaleMode = StageScaleMode.NO_SCALE;
                stage.align = StageAlign.TOP_LEFT;
            }

            _context = new Context();
            _context.install(GameMVCSBundle);
            _context.configure(GameConfig);
            _context.configure(new ContextView(this));
        }
    }
}
