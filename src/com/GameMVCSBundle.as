/**
 * Created by Demon on 20.07.2014.
 */
package com
{
    import robotlegs.bender.extensions.contextView.ContextViewExtension;
    import robotlegs.bender.extensions.contextView.ContextViewListenerConfig;
    import robotlegs.bender.extensions.contextView.StageSyncExtension;
    import robotlegs.bender.extensions.directCommandMap.DirectCommandMapExtension;
    import robotlegs.bender.extensions.enhancedLogging.TraceLoggingExtension;
    import robotlegs.bender.extensions.eventCommandMap.EventCommandMapExtension;
    import robotlegs.bender.extensions.eventDispatcher.EventDispatcherExtension;
    import robotlegs.bender.extensions.localEventMap.LocalEventMapExtension;
    import robotlegs.bender.extensions.mediatorMap.MediatorMapExtension;
    import robotlegs.bender.extensions.signalCommandMap.SignalCommandMapExtension;
    import robotlegs.bender.extensions.viewManager.StageCrawlerExtension;
    import robotlegs.bender.extensions.viewManager.StageObserverExtension;
    import robotlegs.bender.extensions.viewManager.ViewManagerExtension;
    import robotlegs.bender.extensions.vigilance.VigilanceExtension;
    import robotlegs.bender.framework.api.IBundle;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.LogLevel;

    public class GameMVCSBundle implements IBundle
    {
        public function GameMVCSBundle()
        {
        }

        public function extend(context:IContext):void
        {
            context.logLevel = LogLevel.INFO;

            context.install(
                    VigilanceExtension,
                    ContextViewExtension,
                    EventDispatcherExtension,
                    DirectCommandMapExtension,
                    EventCommandMapExtension,
                    LocalEventMapExtension,
                    ViewManagerExtension,
                    StageObserverExtension,
                    MediatorMapExtension,
                    StageCrawlerExtension,
                    StageSyncExtension,
                    TraceLoggingExtension,
                    SignalCommandMapExtension
            );
            context.configure(ContextViewListenerConfig);
        }
    }
}
