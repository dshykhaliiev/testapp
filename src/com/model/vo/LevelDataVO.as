/**
 * Created by dmytro_sh on 11.08.2014.
 */
package com.model.vo
{
    public class LevelDataVO
    {
        public var circlesNum:int;
        public var yellowNum:int;
        public var timeBlueYellow:int;
        public var timeBlue:int;

        public function LevelDataVO()
        {
        }
    }
}
