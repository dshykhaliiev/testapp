/**
 * Created by dmytro_sh on 11.08.2014.
 */
package com.model
{
    import com.controller.signals.LevelPreparedSignal;
    import com.model.interfaces.ILevelModel;
    import com.model.vo.LevelDataVO;

    public class LevelModel implements ILevelModel
    {
        private static var CIRCLES_NUM:Vector.<int> = new <int>[8, 10, 12, 13, 14, 15, 16];
        private static var YELLOW_NUM:Vector.<int> = new <int>[3, 4, 4, 5, 5, 6, 6];
        private static var TIME_MOVING_BOTH:int = 3000;
        private static var TIME_MOVING_BLUE:int = 4000;

        private var _curLevelData:LevelDataVO = new LevelDataVO();
        private var _foundCorrectCircles:int;

        [Inject]
        public var levelPreparedSignal:LevelPreparedSignal;


        public function LevelModel()
        {
            super();
        }

        public function prepareLevelData(level:int):void
        {
            _foundCorrectCircles = 0;

            level--;

            if (level >= CIRCLES_NUM.length)
                level = CIRCLES_NUM.length - 1;

            _curLevelData.circlesNum = CIRCLES_NUM[level];
            _curLevelData.yellowNum = YELLOW_NUM[level];
            _curLevelData.timeBlue = TIME_MOVING_BLUE;
            _curLevelData.timeBlueYellow = TIME_MOVING_BOTH;

            levelPreparedSignal.dispatch();
        }

        public function get curLevelData():LevelDataVO
        {
            return _curLevelData;
        }

        public function get foundCorrectCircles():int
        {
            return _foundCorrectCircles;
        }

        public function set foundCorrectCircles(value:int):void
        {
            _foundCorrectCircles = value;
        }
    }
}
