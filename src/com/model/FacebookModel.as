/**
 * Created by dmytro_sh on 04.11.2014.
 */
package com.model
{
    import com.model.interfaces.IFacebookModel;

    public class FacebookModel extends AbstractModel implements IFacebookModel
    {
        private var _appID:String = "1467596546849209";
        private var _permissions:Array = ["publish_actions"];
        private var _shareLink:String = "https://apps.facebook.com/track-it/";

        public function FacebookModel()
        {
            super();
        }

        public function get appID():String
        {
            return _appID;
        }

        public function get permissions():Array
        {
            return _permissions;
        }

        public function get shareLink():String
        {
            return _shareLink;
        }
    }
}
