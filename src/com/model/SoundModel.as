/**
 * Created by dmytro_sh on 12.11.2014.
 */
package com.model
{
    import com.model.interfaces.ISoundModel;

    public class SoundModel extends AbstractModel implements ISoundModel
    {
        private var _click_snd:String = "click_snd";
        private var _itemRight_snd:String = "itemRight_snd";
        private var _itemWrong_snd:String = "itemWrong_snd";

        public function SoundModel()
        {
            super();
        }

        public function get click_snd():String
        {
            return _click_snd;
        }

        public function get itemRight_snd():String
        {
            return _itemRight_snd;
        }

        public function get itemWrong_snd():String
        {
            return _itemWrong_snd;
        }
    }
}
