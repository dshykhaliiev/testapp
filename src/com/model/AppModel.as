/**
 * Created by dmytro_sh on 13.11.2014.
 */
package com.model
{
    import com.model.enums.AppVersionEnum;
    import com.model.interfaces.IAppModel;

    public class AppModel extends AbstractModel implements IAppModel
    {
//        private var _version:AppVersionEnum = AppVersionEnum.FACEBOOK_VERSION;
        private var _version:AppVersionEnum = AppVersionEnum.FGL_VERSION;
//        private var _version:AppVersionEnum = AppVersionEnum.VK_VERSION;

        public function AppModel()
        {
            super();
        }

        public function get version():AppVersionEnum
        {
            return _version;
        }
    }
}
