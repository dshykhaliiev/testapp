/**
 * Created by dmytro_sh on 22.10.2014.
 */
package com.model
{
    import com.model.interfaces.IAdsModel;

    public class AdsModel extends AbstractModel implements IAdsModel
    {
        // CPMStar
        private var _cpmStarPrePlaySpotID:String = "12976Q986A8C25";
        private var _cpmStarInterLevelSpotID:String = "12977Q0D113A8B";

        public function AdsModel()
        {
            super();
        }

        public function get cpmStarPrePlaySpotID():String
        {
            return _cpmStarPrePlaySpotID;
        }

        public function get cpmStarInterLevelSpotID():String
        {
            return _cpmStarInterLevelSpotID;
        }
    }
}
