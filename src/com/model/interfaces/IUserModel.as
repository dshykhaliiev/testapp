/**
 * Created by Demon on 20.07.2014.
 */
package com.model.interfaces
{
    public interface IUserModel
    {
        function set level(value:int):void;
        function get level():int;

        function set lives(value:int):void;
        function get lives():int;

        function reset():void;
    }
}
