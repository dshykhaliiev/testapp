/**
 * Created by dmytro_sh on 11.08.2014.
 */
package com.model.interfaces
{
    import com.model.vo.LevelDataVO;

    public interface ILevelModel
    {
        function prepareLevelData(level:int):void;

        function get curLevelData():LevelDataVO;

        function get foundCorrectCircles():int;

        function set foundCorrectCircles(value:int):void

    }
}
