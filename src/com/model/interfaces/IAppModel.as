/**
 * Created by dmytro_sh on 13.11.2014.
 */
package com.model.interfaces
{
    import com.model.enums.AppVersionEnum;

    public interface IAppModel
    {
        function get version():AppVersionEnum;
    }
}
