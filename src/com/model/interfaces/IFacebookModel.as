/**
 * Created by dmytro_sh on 04.11.2014.
 */
package com.model.interfaces
{
    public interface IFacebookModel
    {
        function get appID():String;
        function get permissions():Array;
        function get shareLink():String;
    }
}
