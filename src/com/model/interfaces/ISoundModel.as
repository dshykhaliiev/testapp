/**
 * Created by dmytro_sh on 12.11.2014.
 */
package com.model.interfaces
{
    public interface ISoundModel
    {
        function get click_snd():String;
        function get itemRight_snd():String;
        function get itemWrong_snd():String;
    }
}
