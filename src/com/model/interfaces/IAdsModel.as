/**
 * Created by dmytro_sh on 22.10.2014.
 */
package com.model.interfaces
{
    public interface IAdsModel
    {
        function get cpmStarPrePlaySpotID():String;
        function get cpmStarInterLevelSpotID():String;
    }
}
