/**
 * Created by Demon on 20.07.2014.
 */
package com.model
{
    import com.controller.signals.LivesUpdatedSignal;
    import com.model.interfaces.IUserModel;

    public class UserModel extends AbstractModel implements IUserModel
    {
        private const DEFAULT_LIVES:int = 3;

        [Inject]
        public var livesUpdatedSignal:LivesUpdatedSignal;

        private var _level:int = 1; // initial level is 1
        private var _lives:int = DEFAULT_LIVES;

        public function UserModel()
        {
            super();
        }

        public function get level():int
        {
            return _level;
        }

        public function set level(value:int):void
        {
            _level = value;
        }

        public function get lives():int
        {
            return _lives;
        }

        public function set lives(value:int):void
        {
            _lives = value;
            livesUpdatedSignal.dispatch();
        }

        public function reset():void
        {
            _lives = DEFAULT_LIVES;
            _level = 1;
        }
    }
}
