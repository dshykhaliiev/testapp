/**
 * Created by dmytro_sh on 13.11.2014.
 */
package com.model.enums
{
    public class AppVersionEnum
    {
        public static const FGL_VERSION:AppVersionEnum = new AppVersionEnum(0);
        public static const WEB_VERSION:AppVersionEnum = new AppVersionEnum(1);
        public static const FACEBOOK_VERSION:AppVersionEnum = new AppVersionEnum(2);
        public static const VK_VERSION:AppVersionEnum = new AppVersionEnum(3);

        private var _version:int;

        public function AppVersionEnum(version:int)
        {
            _version = version;
        }
    }
}
