/**
 * Created by dmytro_sh on 08.09.2014.
 */
package com.view.mediator
{
    import com.controller.signals.StartGameSignal;
    import com.mediator.BaseMediator;
    import com.model.interfaces.ISoundModel;
    import com.service.sounds.ISoundService;
    import com.view.LevelCompleteView;
    import com.view.events.LevelCompleteViewEvent;

    public class LevelCompleteMediator extends BaseMediator
    {
        [Inject]
        public var startGameSignal:StartGameSignal;

        [Inject]
        public var view:LevelCompleteView;

        [Inject]
        public var soundService:ISoundService;

        [Inject]
        public var soundModel:ISoundModel;

        public function LevelCompleteMediator()
        {
            super();
        }

        override protected function init():void
        {
            addViewListener(LevelCompleteViewEvent.NEXT_BTN_CLICKED, onNextBtnClicked);
        }

        override public function destroy():void
        {
            removeViewListener(LevelCompleteViewEvent.NEXT_BTN_CLICKED, onNextBtnClicked);
        }

        private function onNextBtnClicked(event:LevelCompleteViewEvent):void
        {
            soundService.playSound(soundModel.click_snd);
            view.dispose();
            startGameSignal.dispatch();
        }
    }
}
