/**
 * Created by dmytro_sh on 13.08.2014.
 */
package com.view.mediator
{
    import com.controller.signals.StartGameSignal;
    import com.mediator.BaseMediator;
    import com.model.interfaces.ISoundModel;
    import com.model.interfaces.IUserModel;
    import com.service.sounds.ISoundService;
    import com.view.LevelIntroView;
    import com.view.events.LevelIntroViewEvent;

    public class LevelIntroMediator extends BaseMediator
    {
        [Inject]
        public var userModel:IUserModel;

        [Inject]
        public var view:LevelIntroView;

        [Inject]
        public var startGameSignal:StartGameSignal;

        [Inject]
        public var soundService:ISoundService;

        [Inject]
        public var soundModel:ISoundModel;

        public function LevelIntroMediator()
        {
            super();
        }

        override protected function init():void
        {
            addViewListener(LevelIntroViewEvent.MOUSE_CLICKED, dispatchStartGameSignal);

            view.setLevel(userModel.level);
        }

        private function dispatchStartGameSignal(event:LevelIntroViewEvent):void
        {
            soundService.playSound(soundModel.click_snd);
            startGameSignal.dispatch();
            view.dispose();
        }

        override public function destroy():void
        {
            removeViewListener(LevelIntroViewEvent.MOUSE_CLICKED, dispatchStartGameSignal);
            super.destroy();
        }
    }
}
