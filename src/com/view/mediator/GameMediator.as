/**
 * Created by Demon on 20.07.2014.
 */
package com.view.mediator
{
    import com.controller.signals.CheckUserInputSignal;
    import com.controller.signals.ClearLevelSignal;
    import com.controller.signals.GameOverSignal;
    import com.controller.signals.LevelCompleteSignal;
    import com.controller.signals.LevelPreparedSignal;
    import com.controller.signals.LivesUpdatedSignal;
    import com.controller.signals.RightCircleSelectedSignal;
    import com.controller.signals.WrongCircleSelectedSignal;
    import com.mediator.BaseMediator;
    import com.model.interfaces.ILevelModel;
    import com.model.interfaces.ISoundModel;
    import com.model.interfaces.IUserModel;
    import com.service.sounds.ISoundService;
    import com.view.GameView;
    import com.view.events.GameViewEvent;

    public class GameMediator extends BaseMediator
    {
        [Inject]
        public var userModel:IUserModel;

        [Inject]
        public var levelModel:ILevelModel;

        [Inject]
        public var view:GameView;

        [Inject]
        public var levelPreparedSignal:LevelPreparedSignal;

        [Inject]
        public var checkUserInputSignal:CheckUserInputSignal;

        [Inject]
        public var livesUpdatedSignal:LivesUpdatedSignal;

        [Inject]
        public var gameOverSignal:GameOverSignal;

        [Inject]
        public var rightCircleSelectedSignal:RightCircleSelectedSignal;

        [Inject]
        public var wrongCircleSelectedSignal:WrongCircleSelectedSignal;

        [Inject]
        public var levelCompleteSignal:LevelCompleteSignal;

        [Inject]
        public var clearLevelSignal:ClearLevelSignal;

        [Inject]
        public var soundService:ISoundService;

        [Inject]
        public var soundModel:ISoundModel;

        public function GameMediator()
        {
            super();
        }

        override protected function init():void
        {
            // listen signals from context
            levelPreparedSignal.add(onLevelPrepared);
            livesUpdatedSignal.add(onLivesUpdated);
            gameOverSignal.add(onGameOver);
            rightCircleSelectedSignal.add(onRightCircleSelected);
            wrongCircleSelectedSignal.add(onWrongCircleSelected);
            levelCompleteSignal.add(onLevelComplete);
            clearLevelSignal.add(onClearLevel);

            // listen view for events
            addViewListener(GameViewEvent.CIRCLE_CLICKED, onCircleClicked);
        }


        private function onCircleClicked(event:GameViewEvent):void
        {
            checkUserInputSignal.dispatch(event.circle)
        }

        override public function destroy():void
        {
            levelPreparedSignal.remove(onLevelPrepared);
            livesUpdatedSignal.remove(onLivesUpdated);
            gameOverSignal.remove(onGameOver);
            rightCircleSelectedSignal.remove(onRightCircleSelected);
            wrongCircleSelectedSignal.remove(onWrongCircleSelected);
            levelCompleteSignal.remove(onLevelComplete);
            clearLevelSignal.remove(onClearLevel);

            // listen view for events
            removeViewListener(GameViewEvent.CIRCLE_CLICKED, onCircleClicked);
            super.destroy();
        }

        private function onLevelPrepared():void
        {
            view.createLevel(levelModel.curLevelData);
            view.updateLevel(userModel.level);
            view.updateLives(userModel.lives);
        }

        private function onLivesUpdated():void
        {
            view.updateLives(userModel.lives);
        }

        private function onGameOver():void
        {
            view.finishGame();
        }

        private function onRightCircleSelected():void
        {
            soundService.playSound(soundModel.itemRight_snd);
            view.setCircleCorrect(true);
        }

        private function onWrongCircleSelected():void
        {
            soundService.playSound(soundModel.itemWrong_snd);
            view.setCircleCorrect(false);
        }

        private function onLevelComplete():void
        {
            view.finishGame();
        }

        private function onClearLevel():void
        {
            view.clearLevel();
        }
    }
}
