/**
 * Created by dmytro_sh on 05.09.2014.
 */
package com.view.mediator
{
    import com.controller.signals.FBShareSignal;
    import com.controller.signals.ResetGameSignal;
    import com.mediator.BaseMediator;
    import com.model.enums.AppVersionEnum;
    import com.model.interfaces.IAdsModel;
    import com.model.interfaces.IAppModel;
    import com.model.interfaces.ISoundModel;
    import com.service.ads.CPMStarAdsService;
    import com.service.sounds.ISoundService;
    import com.view.GameOverView;
    import com.view.events.GameOverViewEvent;

    public class GameOverMediator extends BaseMediator
    {
        [Inject]
        public var view:GameOverView;

        [Inject]
        public var resetGameSignal:ResetGameSignal;

        [Inject]
        public var fbShareSignal:FBShareSignal;

        [Inject]
        public var soundService:ISoundService;

        [Inject]
        public var soundModel:ISoundModel;

        [Inject]
        public var adsModel:IAdsModel;

        [Inject]
        public var appModel:IAppModel;

        public function GameOverMediator()
        {
            super();
        }

        override protected function init():void
        {
            addViewListener(GameOverViewEvent.TRY_AGAIN_BTN_CLICKED, onTryAgainBtnClicked);
            addViewListener(GameOverViewEvent.FB_SHARE_BTN_CLICKED, onFBShareBtnClicked);

            if (appModel.version == AppVersionEnum.FGL_VERSION)
                view.hideAdsAndFB();

            view.setAdsID(adsModel.cpmStarInterLevelSpotID);
        }


        override public function destroy():void
        {
            removeViewListener(GameOverViewEvent.TRY_AGAIN_BTN_CLICKED, onTryAgainBtnClicked);
            removeViewListener(GameOverViewEvent.FB_SHARE_BTN_CLICKED, onFBShareBtnClicked);
        }

        private function onTryAgainBtnClicked(event:GameOverViewEvent):void
        {
            soundService.playSound(soundModel.click_snd);
            view.dispose();
            resetGameSignal.dispatch();
        }

        private function onFBShareBtnClicked(event:GameOverViewEvent):void
        {
            soundService.playSound(soundModel.click_snd);
            fbShareSignal.dispatch();
        }
    }
}
