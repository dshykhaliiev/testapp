/**
 * Created by dmytro_sh on 30.07.2014.
 */
package com.view.mediator
{
    import com.controller.signals.ShowLevelIntroSignal;
    import com.mediator.BaseMediator;
    import com.model.enums.AppVersionEnum;
    import com.model.interfaces.IAdsModel;
    import com.model.interfaces.IAppModel;
    import com.model.interfaces.ISoundModel;
    import com.service.sounds.ISoundService;
    import com.view.MenuView;
    import com.view.events.MenuViewEvent;

    import flash.events.Event;
    import flash.media.Sound;

    public class MenuMediator extends BaseMediator
    {
        [Inject]
        public var showLevelIntroSignal:ShowLevelIntroSignal;

        [Inject]
        public var view:MenuView;

        [Inject]
        public var adsModel:IAdsModel;

        [Inject]
        public var soundService:ISoundService;

        [Inject]
        public var soundModel:ISoundModel;

        [Inject]
        public var appModel:IAppModel;

        public function MenuMediator()
        {
            super();
        }

        override protected function init():void
        {
            addViewListener(MenuViewEvent.PLAY_BTN_CLICKED, onPlayBtnClicked);

            switch (appModel.version)
            {
                case AppVersionEnum.FACEBOOK_VERSION:
                case AppVersionEnum.WEB_VERSION:
                    view.setAdsID(adsModel.cpmStarPrePlaySpotID);
                break;
            }
        }

        private function onPlayBtnClicked(event:Event):void
        {
            soundService.playSound(soundModel.click_snd);

            showLevelIntroSignal.dispatch();
            view.dispose();
        }

        override public function destroy():void
        {
            removeViewListener(MenuViewEvent.PLAY_BTN_CLICKED, onPlayBtnClicked);
        }
    }
}
