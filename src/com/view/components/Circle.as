/**
 * Created by dmytro_sh on 14.08.2014.
 */
package com.view.components
{
    import com.utils.MathUtils;

    import flash.display.MovieClip;
    import flash.geom.Vector3D;

    public class Circle extends MovieClip
    {
        private static const MAX_SPEED:int = 7;
        private static const MIN_SPEED:int = 6;

        private var view:Circle_mc;
        private var speed:Vector3D;
        private var _linearSpeed:Number;
        private var _isYellow:Boolean = false;
        private var _inactive:Boolean = false;

        public function Circle(isYellow:Boolean = false)
        {
            _isYellow = isYellow;
            init();
        }

        private function init():void
        {
            view = new Circle_mc();
            addChild(view);

            speed = MathUtils.getRandomUnitVector();
            _linearSpeed = MathUtils.getRandom(MIN_SPEED, MAX_SPEED);

            if (_isYellow)
                view.gotoAndStop(2);
            else
                view.gotoAndStop(1);
        }

        public function setBlueState():void
        {
            view.gotoAndStop(1);
        }

//        public function setYellowState():void
//        {
//            view.gotoAndStop(2);
//        }

        public function update():void
        {
            x += speed.x * _linearSpeed;
            y += speed.y * _linearSpeed;

            _inactive = false;
        }

        public function changeHorDirection():void
        {
            speed.x = speed.x * -1;
//            update();
        }

        public function changeVerDirection():void
        {
            speed.y = speed.y * -1;
//            update();
        }

        public function dispose():void
        {
            _isYellow = false;

            removeChild(view);

            if (parent)
                parent.removeChild(this);
        }

        public function get isYellow():Boolean
        {
            return _isYellow;
        }

        public function setRightState():void
        {
            view.gotoAndStop("right");
        }

        public function setWrongState():void
        {
            view.gotoAndStop("wrong");
        }


        public function get linearSpeed():Number
        {
            return _linearSpeed;
        }
    }
}
