/**
 * Created by dmytro_sh on 28.08.2014.
 */
package com.view.components
{
    import com.view.AssetParser;

    import flash.text.TextField;

    public class LivePanel extends AssetParser
    {
        private const DEFAULT_LIVES_NUM:int = 3;

        public function LivePanel()
        {
            super();
        }


        override protected function onComplete():void
        {
            super.onComplete();
            updateLives(DEFAULT_LIVES_NUM);
        }

        public function updateLives(lives:int):void
        {
            view.gotoAndStop(lives + 1);
        }
    }
}
