/**
 * Created by dmytro_sh on 30.07.2014.
 */
package com.view
{
    import com.service.ads.CPMStarAdsService;
    import com.view.events.MenuViewEvent;

    import flash.display.MovieClip;
    import flash.display.SimpleButton;
    import flash.events.MouseEvent;
    import flash.utils.clearTimeout;
    import flash.utils.setTimeout;

    public class MenuView extends AssetParser
    {
        private static const LINKAGE:String = "MenuView_mc";
        private static const HIDE_ADS_DELAY:int = 20000; // ms

        public var play_btn:SimpleButton;

        private var adsContainer_mc:MovieClip;
        private var adsID:String;
        private var adsTimeout:uint;

        public function MenuView()
        {
            super(LINKAGE);
        }

        public function setAdsID(id:String):void
        {
            adsID = id;

            adsContainer_mc = new MovieClip();
            adsContainer_mc.graphics.beginFill(1);
            adsContainer_mc.graphics.endFill();
            adsContainer_mc.width = 640;
            adsContainer_mc.height = 480;
            adsContainer_mc.mouseEnabled = false;
            adsContainer_mc.mouseChildren = false;
            stage.addChild(adsContainer_mc);

            CPMStarAdsService.showAds(adsContainer_mc, adsID);
            adsTimeout = setTimeout(hideAds, HIDE_ADS_DELAY);
        }

        override protected function onComplete():void
        {
            play_btn.addEventListener(MouseEvent.CLICK, onPlayBtnClick);
            stage.addEventListener(MouseEvent.CLICK, onStageClick);
        }

        override public function dispose():void
        {
            clearTimeout(adsTimeout);
            stage.removeEventListener(MouseEvent.CLICK, onStageClick);
            play_btn.removeEventListener(MouseEvent.CLICK, onPlayBtnClick);
            hideAds();
            super.dispose();
        }

        private function onPlayBtnClick(event:MouseEvent):void
        {
            dispatchEvent(new MenuViewEvent(MenuViewEvent.PLAY_BTN_CLICKED));
        }

        private function onStageClick(event:MouseEvent):void
        {
            stage.removeEventListener(MouseEvent.CLICK, onStageClick);
            CPMStarAdsService.hideAds(adsContainer_mc);
        }

        private function hideAds():void
        {
            if (adsContainer_mc)
            {
                CPMStarAdsService.hideAds(adsContainer_mc);
                adsContainer_mc = null;
            }
        }
    }
}
