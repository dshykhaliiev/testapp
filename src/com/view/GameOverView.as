/**
 * Created by dmytro_sh on 05.09.2014.
 */
package com.view
{
    import com.service.ads.CPMStarAdsService;
    import com.view.events.GameOverViewEvent;

    import flash.display.MovieClip;

    import flash.display.SimpleButton;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    import flash.utils.clearTimeout;
    import flash.utils.setTimeout;

    public class GameOverView extends AssetParser
    {
        private const LINKAGE:String = "GameOverView_mc";

        public var tryAgain_btn:SimpleButton;
        public var fbShare_btn:SimpleButton;
        public var liked_txt:TextField;

        private var adsContainer_mc:MovieClip;
        private var adsTimeout:uint;
        private var btnTimeout:uint;
        private var adsID:String;

        public function GameOverView()
        {
            super(LINKAGE);
        }

        public function hideAdsAndFB():void
        {
            liked_txt.visible = false;
            fbShare_btn.visible = false;
        }

        public function setAdsID(id:String):void
        {
            adsID = id;
            showAds();
        }

        private function showAds():void
        {
            adsContainer_mc = new MovieClip();
            adsContainer_mc.graphics.beginFill(1);
            adsContainer_mc.graphics.endFill();
            adsContainer_mc.width = 640;
            adsContainer_mc.height = 480;
            adsContainer_mc.mouseEnabled = false;
            adsContainer_mc.mouseChildren = false;
            stage.addChild(adsContainer_mc);
            CPMStarAdsService.showAds(adsContainer_mc, adsID);

            adsTimeout = setTimeout(hideAds, 20000);
        }

        override protected function onComplete():void
        {
            super.onComplete();

            tryAgain_btn.addEventListener(MouseEvent.CLICK, onTryAgainBtnClick);
            tryAgain_btn.visible = false;

            btnTimeout = setTimeout(showRetryBtn, 2500);

            fbShare_btn.addEventListener(MouseEvent.CLICK, onFBShareBtnClick);
        }

        override public function dispose():void
        {
            if (btnTimeout)
                clearTimeout(btnTimeout);

            if (adsTimeout)
                clearTimeout(adsTimeout);

            tryAgain_btn.removeEventListener(MouseEvent.CLICK, onTryAgainBtnClick);
            fbShare_btn.removeEventListener(MouseEvent.CLICK, onFBShareBtnClick);
            super.dispose();
        }

        private function onTryAgainBtnClick(event:MouseEvent):void
        {
            dispatchEvent(new GameOverViewEvent(GameOverViewEvent.TRY_AGAIN_BTN_CLICKED));
        }

        private function onFBShareBtnClick(event:MouseEvent):void
        {
            dispatchEvent(new GameOverViewEvent(GameOverViewEvent.FB_SHARE_BTN_CLICKED));
        }

        private function hideAds():void
        {
            if (adsContainer_mc)
            {
                CPMStarAdsService.hideAds(adsContainer_mc);
                adsContainer_mc = null;
            }
        }

        private function showRetryBtn():void
        {
            tryAgain_btn.visible = true;
        }
    }
}
