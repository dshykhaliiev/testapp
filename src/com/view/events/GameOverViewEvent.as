/**
 * Created by dmytro_sh on 09.09.2014.
 */
package com.view.events
{
    import flash.events.Event;

    public class GameOverViewEvent extends Event
    {
        public static const TRY_AGAIN_BTN_CLICKED:String = "GameOverViewEvent:TryAgainBtnClicked";
        public static const FB_SHARE_BTN_CLICKED:String = "GameOverViewEvent:fbShareBtnClicked";

        public function GameOverViewEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
