/**
 * Created by dmytro_sh on 31.07.2014.
 */
package com.view.events
{
    import flash.events.Event;

    public class MenuViewEvent extends Event
    {
        public static const PLAY_BTN_CLICKED:String = "MenuViewEvent::PlayBtnClicked";

        public function MenuViewEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
