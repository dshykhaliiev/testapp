/**
 * Created by dmytro_sh on 19.08.2014.
 */
package com.view.events
{
    import com.view.components.Circle;

    import flash.events.Event;

    public class GameViewEvent extends Event
    {
        public static const CIRCLE_CLICKED:String = "GameViewEvent::CircleClicked";

        private var _circle:Circle;

        public function GameViewEvent(type:String, circleClicked:Circle)
        {
            _circle = circleClicked;
            super(type, false, false);
        }

        public function get circle():Circle
        {
            return _circle;
        }
    }
}
