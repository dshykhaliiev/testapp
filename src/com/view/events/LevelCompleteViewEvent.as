/**
 * Created by dmytro_sh on 08.09.2014.
 */
package com.view.events
{
    import flash.events.Event;

    public class LevelCompleteViewEvent extends Event
    {
        public static const NEXT_BTN_CLICKED:String = "LevelCompleteViewEvent:NextBtnClicked";

        public function LevelCompleteViewEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
