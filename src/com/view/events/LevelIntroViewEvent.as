/**
 * Created by dmytro_sh on 13.08.2014.
 */
package com.view.events
{
    import flash.events.Event;

    public class LevelIntroViewEvent extends Event
    {
        public static const MOUSE_CLICKED:String = "LevelIntroViewEvent::MouseClicked";

        public function LevelIntroViewEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
