/**
 * Created by dmytro_sh on 08.09.2014.
 */
package com.view
{
    import com.view.events.LevelCompleteViewEvent;

    import flash.display.SimpleButton;
    import flash.events.MouseEvent;

    public class LevelCompleteView extends AssetParser
    {
        private const LINKAGE:String = "LevelCompleteView_mc";

        public var next_btn:SimpleButton;

        public function LevelCompleteView()
        {
            super(LINKAGE);
        }

        override protected function onComplete():void
        {
            next_btn.addEventListener(MouseEvent.CLICK, onNextBtnClick);
        }

        override public function dispose():void
        {
            next_btn.removeEventListener(MouseEvent.CLICK, onNextBtnClick);
            super.dispose();
        }

        private function onNextBtnClick(event:MouseEvent):void
        {
            dispatchEvent(new LevelCompleteViewEvent(LevelCompleteViewEvent.NEXT_BTN_CLICKED));
        }
    }
}
