/**
 * Created by Demon on 20.07.2014.
 */
package com.view
{
    import com.model.vo.LevelDataVO;
    import com.utils.MathUtils;
    import com.view.components.Circle;
    import com.view.components.LivePanel;
    import com.view.events.GameViewEvent;

    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.text.TextField;
    import flash.utils.setTimeout;

    public class GameView extends AssetParser
    {
        private static const LINKAGE:String = "GameView_mc";
        private static const CIRCLES_MINIMUM_DISTANCE:int = 10;

        public var field_mc:MovieClip;
        public var instr_mc:MovieClip;
        public var level_txt:TextField;

        public var livePanel:LivePanel;

        private var map:Vector.<Point> = new <Point>[];
        private var tempMap:Vector.<Point>;
        private var circles:Vector.<Circle> = new <Circle>[];
        private var levelData:LevelDataVO;
        private var selectedCircle:Circle;

        private var hideYellowTimeout:int;
        private var stopTimeOut:int;

        public function GameView()
        {
            super(LINKAGE);
        }


        override protected function onComplete():void
        {
            super.onComplete();
            createMap();
            showInstr(false);
        }

        public function createLevel(data:LevelDataVO):void
        {
            levelData = data;

            createCircles();
            startLevel();
        }

        public function updateLevel(level:int):void
        {
            level_txt.text = "Level: " + level;
        }

        public function updateLives(lives:int):void
        {
            livePanel.updateLives(lives);
        }

        public function finishGame():void
        {
            removeListeners();
        }

        public function setCircleCorrect(value:Boolean):void
        {
            if (value)
                selectedCircle.setRightState();
            else
                selectedCircle.setWrongState();
        }

        public function clearLevel():void
        {
            tempMap = null;

            var circle:Circle;
            while (circles.length > 0)
            {
                circle = circles.pop();
                circle.removeEventListener(MouseEvent.CLICK, onCircleClick);
                circle.dispose();
                circle = null;
            }

        }


        override public function dispose():void
        {
            clearLevel();
        }

        private function addListeners():void
        {
            for each(var circle:Circle in circles)
            {
                circle.addEventListener(MouseEvent.CLICK, onCircleClick);
            }
        }

        private function removeListeners():void
        {
            for each(var circle:Circle in circles)
            {
                circle.removeEventListener(MouseEvent.CLICK, onCircleClick);
            }
        }

        private function createMap():void
        {
            var i:int;
            var j:int;
            var circle:Circle = new Circle();
            var cellSize:int = circle.width * 3;//+ CIRCLES_MINIMUM_DISTANCE * 3;
            circle.dispose();

            var mapCols:int = Math.floor(field_mc.width / cellSize);
            var mapRows:int = Math.floor(field_mc.height / cellSize);

            for (i = 0; i < mapRows; i++)
            {
                for (j = 0; j < mapCols; j++)
                {
                    var x:int = field_mc.x + j * cellSize + cellSize * .5;
                    var y:int = field_mc.y + i * cellSize + cellSize * .5;
                    map.push(new Point(x, y));
                }
            }

        }

        private function createCircles():void
        {
            var i:int;
            var point:Point;
            var isYellow:Boolean;
            var circle:Circle;

            tempMap = new <Point>[];
            tempMap = map.concat();

            for (i = 0; i < levelData.circlesNum; i++)
            {
                point = getRandomCell();
                isYellow = (i >= levelData.yellowNum) ? false : true;

                circle = new Circle(isYellow);
                circle.x = point.x;
                circle.y = point.y;
                addChild(circle);
                circles.push(circle);
            }


        }

        private function getRandomCell():Point
        {
            var index:int = MathUtils.getRandom(0, tempMap.length);
            var point:Point = tempMap[index];
            tempMap.splice(index, 1);

            return point;
        }

        private function startLevel():void
        {
            showInstr(false);
            addEventListener(Event.ENTER_FRAME, onEnterFrame);
            hideYellowTimeout = setTimeout(hideYellowCircles, levelData.timeBlueYellow);
        }

        private function hideYellowCircles():void
        {
            var circle:Circle;
            for each (circle in circles)
            {
                if (circle.isYellow)
                    circle.setBlueState();
            }
            stopTimeOut = setTimeout(stopCircles, levelData.timeBlue);

        }

        private function stopCircles():void
        {
            removeEventListener(Event.ENTER_FRAME, onEnterFrame);
            askUserInput();
        }

        private function onEnterFrame(event:Event):void
        {
            var circle:Circle;
            var i:int;
            var j:int;
            var len:int = circles.length;

            for (i = 0; i < len; i++)
            {
                circle = circles[i];
                circle.update();
                // check limits
                // check right
                if (circle.x >= field_mc.x + field_mc.width - circle.width * .5)
                {
                    circle.x = field_mc.x + field_mc.width - circle.width * .5;
                    circle.changeHorDirection();
                }

                // check left
                if (circle.x <= field_mc.x + circle.width * .5)
                {
                    circle.x = field_mc.x + circle.width * .5;
                    circle.changeHorDirection()
                }

                if (circle.y >= field_mc.y + field_mc.height - circle.height * .5)
                {
                    circle.y = field_mc.y + field_mc.height - circle.height * .5;
                    circle.changeVerDirection();
                }

                if (circle.y <= field_mc.y + circle.height * .5)
                {
                    circle.y = field_mc.y + circle.height * .5;
                    circle.changeVerDirection();
                }

                // check other circles
                for (j = i + 1; j < len; j++)
                {
                    if (MathUtils.getDistance(circle.x, circle.y, circles[j].x, circles[j].y) < circle.width + CIRCLES_MINIMUM_DISTANCE)
                    {
                        circles[j].changeHorDirection();
                        circles[j].changeVerDirection();
                        circles[j].update();

                        circle.changeHorDirection();
                        circle.changeVerDirection();
                        circle.update();

                        if (circles[j].linearSpeed > circle.linearSpeed)
                            circles[j].update();
                        else
                            circle.update();
                    }
                }

            }
        }

        private function askUserInput():void
        {
            addListeners();
            showInstr(true);
        }

        private function showInstr(value:Boolean):void
        {
            instr_mc.visible = value;
            instr_mc.gotoAndStop(1);

            if (value)
                instr_mc.play();
        }

        private function onCircleClick(event:MouseEvent):void
        {
            selectedCircle = event.currentTarget as Circle;
            selectedCircle.removeEventListener(MouseEvent.CLICK, onCircleClick);
            dispatchEvent(new GameViewEvent(GameViewEvent.CIRCLE_CLICKED, event.currentTarget as Circle));
        }
    }
}
