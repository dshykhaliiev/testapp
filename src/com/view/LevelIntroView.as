/**
 * Created by dmytro_sh on 06.08.2014.
 */
package com.view
{
    import com.view.events.LevelIntroViewEvent;

    import fl.transitions.Tween;
    import fl.transitions.easing.None;

    import flash.events.MouseEvent;
    import flash.text.TextField;

    public class LevelIntroView extends AssetParser
    {
        private static const LINKAGE:String = "LevelIntroView_mc";

        public var level_txt:TextField;

        private var tween:Tween;

        public function LevelIntroView()
        {
            super(LINKAGE);
        }

        override protected function onComplete():void
        {
            addEventListener(MouseEvent.CLICK, onMouseClick);
            tween = new Tween(view, "alpha", None.easeIn, 0, 1, 3);
            tween.start();

            var tw:Tween = new Tween(view, "y", None.easeIn, -view.height, 0, 5);
            tw.start();
        }

        private function onMouseClick(event:MouseEvent):void
        {
            dispatchEvent(new LevelIntroViewEvent(LevelIntroViewEvent.MOUSE_CLICKED));
        }


        public function setLevel(level:int):void
        {
            level_txt.text = "Level " + level.toString();
        }


        override public function dispose():void
        {
            removeEventListener(MouseEvent.CLICK, onMouseClick);
            super.dispose();
        }
    }
}
