/**
 * Created by dmytro_sh on 08.09.2014.
 */
package com.controller.commands
{
    import com.model.interfaces.IUserModel;
    import com.view.LevelCompleteView;

    import flash.utils.setTimeout;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    public class LevelCompleteCommand extends Command
    {
        private static const SHOW_LEVEL_COMPLETE_DELAY:int = 600;

        [Inject]
        public var contextView:ContextView;

        [Inject]
        public var userModel:IUserModel;

        public function LevelCompleteCommand()
        {
            super();
        }

        override public function execute():void
        {
            setTimeout(showLevelCompleteView, SHOW_LEVEL_COMPLETE_DELAY);
        }

        private function showLevelCompleteView():void
        {
            var view:LevelCompleteView = new LevelCompleteView();
            contextView.view.addChild(view);

            userModel.level++;
        }
    }
}
