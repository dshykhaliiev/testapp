/**
 * Created by dmytro_sh on 05.09.2014.
 */
package com.controller.commands
{
    import com.view.GameOverView;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    public class GameOverCommand extends Command
    {
        [Inject]
        public var contextView:ContextView;

        public function GameOverCommand()
        {
            super();
        }

        override public function execute():void
        {
            var gameOverView:GameOverView = new GameOverView();
            contextView.view.addChild(gameOverView);
        }
    }
}
