/**
 * Created by dmytro_sh on 19.08.2014.
 */
package com.controller.commands
{
    import com.controller.signals.CheckUserInputSignal;
    import com.controller.signals.GameOverSignal;
    import com.controller.signals.LevelCompleteSignal;
    import com.controller.signals.RightCircleSelectedSignal;
    import com.controller.signals.WrongCircleSelectedSignal;
    import com.model.interfaces.ILevelModel;
    import com.model.interfaces.IUserModel;
    import com.view.components.Circle;

    import flash.utils.setTimeout;

    import robotlegs.bender.bundles.mvcs.Command;

    public class CheckUserInputCommand extends Command
    {
        [Inject]
        public var circleClicked:Circle;

        [Inject]
        public var checkUserInputSignal:CheckUserInputSignal;

        [Inject]
        public var gameOverSignal:GameOverSignal;

        [Inject]
        public var rightCircleSelectedSignal:RightCircleSelectedSignal;

        [Inject]
        public var wrongCircleSelectedSignal:WrongCircleSelectedSignal;

        [Inject]
        public var levelCompleteSignal:LevelCompleteSignal;

        [Inject]
        public var userModel:IUserModel;

        [Inject]
        public var levelModel:ILevelModel;

        public function CheckUserInputCommand()
        {
            super();
        }

        override public function execute():void
        {
            if (circleClicked.isYellow)
            {
                rightCircleSelectedSignal.dispatch();
                addFoundCorrectCircle();
                // TODO: play sound
            }
            else
            {
                wrongCircleSelectedSignal.dispatch();
                decreaseLives();
                // TODO: play sound
            }
        }

        private function decreaseLives():void
        {
            userModel.lives -= 1;
            if (userModel.lives == 0)
                gameOverSignal.dispatch();
        }

        private function addFoundCorrectCircle():void
        {
            levelModel.foundCorrectCircles++;

            if (levelModel.foundCorrectCircles == levelModel.curLevelData.yellowNum)
                levelCompleteSignal.dispatch();
        }
    }
}
