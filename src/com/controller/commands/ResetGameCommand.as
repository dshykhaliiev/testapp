/**
 * Created by dmytro_sh on 09.09.2014.
 */
package com.controller.commands
{
    import com.controller.signals.StartGameSignal;
    import com.model.interfaces.IUserModel;

    import robotlegs.bender.bundles.mvcs.Command;

    public class ResetGameCommand extends Command
    {
        [Inject]
        public var startGameSignal:StartGameSignal;

        [Inject]
        public var userModel:IUserModel;

        public function ResetGameCommand()
        {
            super();
        }

        override public function execute():void
        {
            userModel.reset();
            startGameSignal.dispatch();
        }
    }
}
