/**
 * Created by dmytro_sh on 13.08.2014.
 */
package com.controller.commands
{
    import com.view.GameView;
    import com.view.LevelIntroView;

    import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;

    public class ShowLevelIntroCommand extends Command
    {
        [Inject]
        public var contextView:ContextView;

        public function ShowLevelIntroCommand()
        {
            super();
        }

        override public function execute():void
        {
            var gameView:GameView = new GameView();
            contextView.view.addChild(gameView);

            var levelIntroView:LevelIntroView = new LevelIntroView();
            contextView.view.addChild(levelIntroView);
        }
    }
}
