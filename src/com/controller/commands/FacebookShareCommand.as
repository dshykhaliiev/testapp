/**
 * Created by dmytro_sh on 04.11.2014.
 */
package com.controller.commands
{
    import com.log;
    import com.model.interfaces.IFacebookModel;
    import com.service.social.facebook.IFacebookService;

    import robotlegs.bender.bundles.mvcs.Command;

    public class FacebookShareCommand extends Command
    {
        [Inject]
        public var facebookService:IFacebookService;

        [Inject]
        public var facebookModel:IFacebookModel;

        override public function execute():void
        {
            facebookService.init(facebookModel.appID);
            facebookService.share(facebookModel.shareLink);
        }
    }
}
