/**
 * Created by dmytro_sh on 31.07.2014.
 */
package com.controller.commands
{
    import com.controller.signals.ClearLevelSignal;
    import com.model.interfaces.ILevelModel;
    import com.model.interfaces.IUserModel;

    import robotlegs.bender.bundles.mvcs.Command;

    public class StartGameCommand extends Command
    {
        [Inject]
        public var userModel:IUserModel;

        [Inject]
        public var levelModel:ILevelModel;

        [Inject]
        public var clearLevelSignal:ClearLevelSignal;

        public function StartGameCommand()
        {
            super();
        }

        override public function execute():void
        {
            prepareLevel();
        }

        private function prepareLevel():void
        {
            clearLevelSignal.dispatch();
            levelModel.prepareLevelData(userModel.level);
        }
    }
}
