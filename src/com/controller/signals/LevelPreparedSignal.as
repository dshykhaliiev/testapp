/**
 * Created by dmytro_sh on 13.08.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.ISignal;
    import org.osflash.signals.Signal;

    public class LevelPreparedSignal extends Signal implements ISignal
    {
        public function LevelPreparedSignal(...rest)
        {
            super(rest);
        }
    }
}
