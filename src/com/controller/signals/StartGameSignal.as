/**
 * Created by dmytro_sh on 31.07.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class StartGameSignal extends Signal
    {
        public function StartGameSignal(...rest)
        {
            super(rest);
        }
    }
}
