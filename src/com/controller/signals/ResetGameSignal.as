/**
 * Created by dmytro_sh on 09.09.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class ResetGameSignal extends Signal
    {
        public function ResetGameSignal(...rest)
        {
            super(rest);
        }
    }
}
