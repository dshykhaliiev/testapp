/**
 * Created by dmytro_sh on 01.09.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class LivesUpdatedSignal extends Signal
    {
        public function LivesUpdatedSignal(...rest)
        {
            super(rest);
        }
    }
}
