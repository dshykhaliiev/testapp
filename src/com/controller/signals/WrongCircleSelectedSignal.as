/**
 * Created by dmytro_sh on 05.09.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class WrongCircleSelectedSignal extends Signal
    {
        public function WrongCircleSelectedSignal(...rest)
        {
            super(rest);
        }
    }
}
