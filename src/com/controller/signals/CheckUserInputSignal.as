/**
 * Created by dmytro_sh on 19.08.2014.
 */
package com.controller.signals
{
    import com.view.components.Circle;

    import org.osflash.signals.Signal;

    public class CheckUserInputSignal extends Signal
    {
        public function CheckUserInputSignal()
        {
            super(Circle);
        }
    }
}
