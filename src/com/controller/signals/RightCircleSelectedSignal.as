/**
 * Created by dmytro_sh on 05.09.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class RightCircleSelectedSignal extends Signal
    {
        public function RightCircleSelectedSignal(...rest)
        {
            super(rest);
        }
    }
}
