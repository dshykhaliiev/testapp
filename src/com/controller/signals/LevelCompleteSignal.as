/**
 * Created by dmytro_sh on 08.09.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class LevelCompleteSignal extends Signal
    {
        public function LevelCompleteSignal(...rest)
        {
            super(rest);
        }
    }
}
