/**
 * Created by dmytro_sh on 13.08.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class ShowLevelIntroSignal extends Signal
    {
        public function ShowLevelIntroSignal(...rest)
        {
            super(rest);
        }
    }
}
