/**
 * Created by dmytro_sh on 11.11.2014.
 */
package com.controller.signals
{
    import org.osflash.signals.Signal;

    public class FBShareSignal extends Signal
    {
        public function FBShareSignal(...rest)
        {
            super(rest);
        }
    }
}
