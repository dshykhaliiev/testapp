// load the my-script-file.js and display an alert dialog once the script has been loaded
loadScript('https://connect.facebook.net/en_US/all.js'); // download facebook JDK

function facebookInit( id )
{		
	FB.init({
	    appId      : id // App ID from the App Dashboard
	});
}

function facebookApiCallback()
{

}

function facebookApi(request, type, params)
{
    FB.api( request, type, params, facebookApiCallback)
}

function facebookShare(href)
{
    console.log("share: ", href)
    FB.ui( {    method: 'share_open_graph',
                action_type: 'og.likes',
                action_properties: JSON.stringify({object:href}) } )
}


function loginStatusResponse(response)
{
    console.log("Status: ", response.status)
    if (response.status === 'connected')
    {
        // the user is logged in and has authenticated your
        // app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed
        // request, and the time the access token
        // and signed request each expire
        console.log("User is logged in already")
        var uid = response.authResponse.userID;
        var accessToken = response.authResponse.accessToken;
    }
    else if (response.status === 'not_authorized')
    {
        // the user is logged in to Facebook,
        // but has not authenticated your app
        console.log("facebook login")
        FB.login( facebookLoginResponse, { scope: permissions } )
    }
    else
    {
        console.log("facebook login")
        FB.login( facebookLoginResponse, { scope: permissions } )
        // the user isn't logged in to Facebook.
    }
}

function getLoginStatus()
{
    console.log("Get login status")
    FB.getLoginStatus(loginStatusResponse)
}

function facebookLogin( permissions )
{
    getLoginStatus();
}

function facebookLoginResponse( response )
{
    console.log("facebook login response: ", response)
	if (response.authResponse) 
	{
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response)
		{
			console.log('Your name is ' + response.name);
			document.getElementById("Track-it").onFBInitComplete(response.name);
		});
	}
    else
	{
		console.log('User cancelled login or did not fully authorize.');
	}
}



// Load script function
var script
var done
var loadScriptCallback

function onLoadScript()
{
    if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete'))
    {
        done = true;
        script.onload = null;
        script.onreadystatechange = null;

        if (loadScriptCallback)
        {
            loadScriptCallback();
        }
    }
}


function loadScript(src, callback)
{
    loadScriptCallback = callback;
    done = false;

    var head = document.getElementsByTagName('head')[0];

    script = document.createElement('script');
    script.setAttribute('src', src);
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('charset', 'utf-8');
    script.onload = onLoadScript
    script.onreadstatechange = onLoadScript

    head.insertBefore(script, head.firstChild);
}

