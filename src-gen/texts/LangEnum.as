package texts
{
    public class LangEnum
    {
        public static const EN:LangEnum = new LangEnum("en");

        private var _lang:String;

        public function LangEnum(lang:String)
        {
            _lang = lang;
        }
    }
}